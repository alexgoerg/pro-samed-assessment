$(document).ready(function()    {

    function putComment() {
        inputFieldCommentName = $(this).prev();
        commentID = inputFieldCommentName.attr("commentID");
        commentName = inputFieldCommentName.val();
        $.ajax({
            url: 'php/CommentController.php',
            type: 'PUT',
            data: { commentID: commentID,
                    commentName: commentName},
            success: function(body){
                body = JSON.parse(body);
                status = body.status;
                data = body.data;
                if (status === "success") {
                    inputFieldCommentName.val(data.name);
                    inputFieldCommentName.css("border-color", "green");
                    setTimeout(() => {
                        inputFieldCommentName.css("border-color", "");
                    }, 2000);
                } else {
                    inputFieldCommentName.css("border-color", "red");
                    setTimeout(() => {
                        inputFieldCommentName.css("border-color", "");
                    }, 2000);
                }
            },
            error: function(data) {
            }
        });
    }
    $('.put').click(putComment);

    function deleteComment() {
        inputFieldCommentName = $(this).prev().prev();
        commentID = inputFieldCommentName.attr("commentID");
        console.log(commentID);
        $.ajax({
            url: 'php/CommentController.php',
            type: 'DELETE',
            data: { commentID: commentID},
            success: function(body){
                body = JSON.parse(body);
                status = body.status;
                if (status === "success") {
                    inputFieldCommentName.parent().remove();
                }
            }
        });
    }
    $('.delete').click(deleteComment);


    function postComment(){
        $('.post').click(function() {
            inputFieldCommentName = $(this).prev();
            commentName = inputFieldCommentName.val();
            console.log(commentName);
            $.ajax({
                url: 'php/CommentController.php',
                type: 'POST',
                data: { commentName: commentName},
                success: function(body){
                    body = JSON.parse(body);
                    status = body.status;
                    data = body.data;
                    if(status === 'success') {
                        txt  = "<div>";
                            txt += "<input commentID='" + data.id +  "' value='" + data.name + "'/>";
                            txt += "<button class='put'>speichern</button>";
                            txt += "<button class='delete'>löschen</button>";
                        txt += "</div>";
                        $('.comments').append(txt);
                        $('.put').unbind('click', putComment);
                        $('.put').bind('click', putComment);
                        $('.delete').unbind('click', deleteComment);
                        $('.delete').bind('click', deleteComment);
                        inputFieldCommentName.val("");
                    }
                }
            });
        });
    }
    postComment();
});