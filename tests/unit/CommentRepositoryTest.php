<?php
    include_once "php/DB.php";
    include_once "php/CommentRepository.php";

    class CommentRepositoryTest extends \PHPUnit\Framework\TestCase {
        public function testTrueEqualsTrue () {
            $this->assertEquals(true, true);
        }

        public function testTrueNotEqualsTrue () {
            $this->assertNotEquals(true, false);
        }

        public function testCommentRepositoryPutComment() {
            $db = new DB;
            $pdo = $db->getPDO();
            $commentRepository = new CommentRepository;
            $this->assertEquals(true, $commentRepository->putComment($pdo, 1, 1));
            // $this->assertEquals(false, $commentRepository->putComment($pdo, 'a', 'test'));
        }

        public function testCommentRepositoryDeleteComment() {
            $db = new DB;
            $pdo = $db->getPDO();
            $commentRepository = new CommentRepository;
            $this->assertEquals(true, $commentRepository->deleteComment($pdo, -1));
            // $this->assertEquals(false, $commentRepository->deleteComment($pdo, 'a'));
        }

        public function testCommentRepositoryPostComment() {
            $db = new DB;
            $pdo = $db->getPDO();
            $commentRepository = new CommentRepository;
            $this->assertGreaterThan(0, $commentRepository->postComment($pdo, 'a'));
        }
    }

?>