<?php
    include_once "php/DB.php";
    include_once "php/CommentService.php";

    class CommentServiceTest extends \PHPUnit\Framework\TestCase {

        public function testCommentServicePutComment() {
            $db = new DB;
            $pdo = $db->getPDO();
            $commentService = new CommentService;
            $this->assertIsArray($commentService->putComment($pdo, 93, 'test'));
            // $this->assertEquals(false, $commentService->putComment($pdo, 'a', 'test'));
        }

        public function testCommentServiceDeleteComment() {
            $db = new DB;
            $pdo = $db->getPDO();
            $commentService = new CommentService;
            $this->assertEquals(true, $commentService->deleteComment($pdo, 1));
            // $this->assertEquals(false, $commentService->deleteComment($pdo, 'a'));
        }

        public function testCommentRepositoryPostComment() {
            $db = new DB;
            $pdo = $db->getPDO();
            $commentService = new CommentService;
            $this->assertIsArray($commentService->postComment($pdo, 'a'));
        }
    }

?>