<?php
    include 'php/DB.php';
    include 'php/CommentService.php';
    $db = new DB;
    $pdo = $db->getPDO();
    $commentService = new CommentService;
?>

<html>
    <head>
        <!-- <meta http-equiv="refresh" content="5"> -->
        <script src="js/jquery-3.6.0.min.js"></script>
        <script src="js/comment.js"></script>
    </head>
    <body>
        <div style="width:350px; margin:100px auto 0px auto; border: 0px solid black;">
            <h2 style="border: 0px solid black;">Kommentare:</h2>
                <?php
                    echo "<div>";
                        echo "<input/>";
                        echo "<button class='post'>anlegen</button>";
                    echo "</div>";
                    echo "<br/>";
                    echo "<div class='comments'>";
                        foreach ($commentService->getComments($pdo) as $row) {
                            echo "<div>";
                                echo "<input commentID=" . $row['id']. " value='" .$row['name'] . "'/>";
                                echo "<button class='put'>speichern</button>";
                                echo "<button class='delete'>löschen</button>";
                            echo "</div>";
                        }
                    echo "</div>";
                ?>
        </div>
    </body>
</html>
