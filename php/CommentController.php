<?php
    include 'DB.php';
    include_once 'CommentService.php';

    if($_SERVER['REQUEST_METHOD'] === "PUT") {
        parse_str(file_get_contents("php://input"),$post_vars);
        $commentID = $post_vars['commentID'];
        $commentName = $post_vars['commentName'];   
        //
        $db = new DB;
        $pdo = $db->getPDO();
        $commentService = new CommentService;
        $result = $commentService->putComment($pdo, $commentID, $commentName);
        //
        if($result !== false) {
            echo json_encode(array("status"=> "success", "data" => array("id" => $result['id'], "name" => $result['name'])));
        } else {
            echo json_encode(array("status"=> "error"));
        }
    }

    if($_SERVER['REQUEST_METHOD'] === "DELETE") {
        parse_str(file_get_contents("php://input"),$post_vars);
        $commentID = $post_vars['commentID'];
        //
        $db = new DB;
        $pdo = $db->getPDO();
        $commentService = new CommentService;
        $result = $commentService->deleteComment($pdo, $commentID);
        //
        if($result === true) {
            echo json_encode(array("status"=> "success"));
        } else {
            echo json_encode(array("status"=> "error"));
        }
    }

    if($_SERVER['REQUEST_METHOD'] === "POST") {
        parse_str(file_get_contents("php://input"),$post_vars);
        $commentName = $post_vars['commentName'];
        //
        $db = new DB;
        $pdo = $db->getPDO();
        $commentService = new CommentService;
        $result = $commentService->postComment($pdo, $commentName);
        //
        if($result !== false) {
            echo json_encode(array("status"=> "success", "data" => array("id" => $result['id'], "name" => $result['name'])));
        } else {
            echo json_encode(array("status"=> "error"));
        }
    }
?>