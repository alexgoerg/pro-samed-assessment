<?php
    include_once 'CommentRepository.php';

    class CommentService {

        function getComments($pdo) {
            $commentRepository = new CommentRepository;
            return $commentRepository->getComments($pdo);
        }

        function putComment($pdo, $commentID, $commentName) {
            $pdo->beginTransaction();
            //
            $commentRepository = new CommentRepository;
            //
            $bResult = $commentRepository->putComment($pdo, $commentID, $commentName);
            //
            if ($bResult === true) {
                $result = $commentRepository->getComment($pdo, $commentID);
                // var_dump($result);
                if($result['name'] === $commentName) {
                    $pdo->commit();
                    return $result;
                } else {
                    $pdo->rollBack();
                    return false;
                }
            } else {
                $pdo->rollBack();
                return false;
            }
        }

        function deleteComment($pdo, $commentID) {
            $pdo->beginTransaction();
            //
            $commentRepository = new CommentRepository;
            //
            $bResult = $commentRepository->deleteComment($pdo, $commentID);
            //
            if($bResult === true) {
                $result = $commentRepository->getComment($pdo, $commentID);
                if($result === false) {
                    $pdo->commit();
                    return true;
                } else {
                    $pdo->rollBack();
                    return false;
                }
            }
        }

        function postComment($pdo, $commentName){
            $pdo->beginTransaction();
            //
            $commentRepository = new CommentRepository;
            //
            $commentID = $commentRepository->postComment($pdo, $commentName);
            //
            if($commentID !== false) {
                $result = $commentRepository->getComment($pdo, $commentID);
                if($result !== false) {
                    $pdo->commit();
                    return $result;
                } else {
                    $pdo->rollBack();
                    return false;
                }
            } else {
                $pdo->rollBack();
                return false;
            }
        }
    }

?>