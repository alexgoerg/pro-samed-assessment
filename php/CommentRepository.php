<?php

    class CommentRepository {

        function getComments($pdo) {
            $sql = "SELECT * FROM comment";
            return $pdo->query($sql);
        }

        function getComment($pdo, $id) {
            $sql = "SELECT * FROM comment WHERE id = " . $id;
            return $pdo->query($sql)->fetch();
        }

        function putComment($pdo, $id, $name) {
            try {
                $sql = "UPDATE comment SET name = :name WHERE id = :id";
                $stmt= $pdo->prepare($sql);
                $stmt->bindParam(':name', $name);
                $stmt->bindParam(':id', $id, PDO::PARAM_INT);
                return $stmt->execute();
            } catch (Exception $e) {
                return false;
            }
        }

        function deleteComment($pdo, $id) {
            try {
                $sql = "DELETE FROM comment WHERE id = :id";
                $stmt= $pdo->prepare($sql);
                $stmt->bindParam(':id', $id, PDO::PARAM_INT);
                return $stmt->execute();
            } catch (Exception $e) {
                return false;
            }
        }

        function postComment($pdo, $name) {
            try {
                $sql = "INSERT INTO comment (name) VALUES (:name)";
                $stmt= $pdo->prepare($sql);
                $stmt->bindParam(':name', $name);
                $stmt->execute();
                $id = $pdo->lastInsertId();
                return $id;
            } catch (Exception $e) {
                return false;
            }
        }
    }

?>