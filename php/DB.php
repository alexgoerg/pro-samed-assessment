<?php

    class DB {

        function getPDO() {
            $pdo = new PDO(
                'mysql:host=localhost:3306;dbname=prosamed',
                'root',
                ''
            );
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            return $pdo;
        }
    }

?>